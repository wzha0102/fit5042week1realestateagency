package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception {
    	propertyRepository.addProperty(new Property(1, "abc st, def", 2, 50, 300.00));
    	propertyRepository.addProperty(new Property(2, "abc st, def", 2, 50, 300.00));
    	propertyRepository.addProperty(new Property(3, "abc st, def", 2, 50, 300.00));
    	propertyRepository.addProperty(new Property(4, "abc st, def", 2, 50, 300.00));
    	propertyRepository.addProperty(new Property(5, "abc st, def", 2, 50, 300.00));
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception{
    	Optional.ofNullable(propertyRepository)
    		.ifPresent(p -> {
				try {
					p.getAllProperties()
							.forEach(property -> System.out.println(property.toString() + "\n"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception {
    	System.out.println("Please enter an ID you want to search: ");
    	int id = new Scanner(System.in).nextInt();
    	Optional.ofNullable(propertyRepository.searchPropertyById(id))
    		.ifPresent(p -> System.out.println(p.toString()));
        
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
